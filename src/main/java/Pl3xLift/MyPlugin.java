package Pl3xLift;

import java.util.List;

import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlift.runnables.StartMetrics;
import net.pl3x.pl3xlift.runnables.TeleportPlayer;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_EventInfo;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.MC_Sign;
import PluginReference.MC_World;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private MC_Server server;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xLift";
		info.version = "0.2";
		info.description = "An elevator plugin using signs";
		return info;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
		Pl3xLibs.getLogger(getPluginInfo()).info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		Pl3xLibs.getLogger(getPluginInfo()).info("Plugin disabled.");
	}

	@Override
	public void onAttemptPlaceOrInteract(MC_Player player, MC_Location loc, MC_EventInfo ei, MC_DirectionNESWUD dir) {
		MC_Sign sign = Pl3xLibs.getWorld(loc.dimension).getSignAt(loc);
		if (sign == null) {
			return;
		}
		List<String> lines = sign.getLines();
		MC_Location playerLoc = player.getLocation();
		if (lines.get(0).equalsIgnoreCase("[lift up]")) {
			MC_Location signLoc = getSignLoc(player.getWorld(), (int) loc.x, (int) loc.y, (int) loc.z, 1);
			if (signLoc == null) {
				return;
			}
			MC_Location teleLoc = new MC_Location(playerLoc.x, signLoc.y - 1, playerLoc.z, playerLoc.dimension, playerLoc.yaw, playerLoc.pitch);
			if (!safeLoc(teleLoc)) {
				return;
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(player, teleLoc), 1);
			return;
		}
		if (lines.get(0).equalsIgnoreCase("[lift down]")) {
			MC_Location signLoc = getSignLoc(player.getWorld(), (int) loc.x, (int) loc.y, (int) loc.z, -1);
			if (signLoc == null) {
				return;
			}
			MC_Location teleLoc = new MC_Location(playerLoc.x, signLoc.y - 1, playerLoc.z, playerLoc.dimension, playerLoc.yaw, playerLoc.pitch);
			if (!safeLoc(teleLoc)) {
				return;
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(player, teleLoc), 1);
			return;
		}
	}

	private MC_Location getSignLoc(MC_World world, int x, int y, int z, int i) {
		for (int j = y + i; j < server.getMaxBuildHeight() && j >= 0; j += i) {
			MC_Location signLoc = new MC_Location(x, j, z, world.getDimension(), 0, 0);
			MC_Sign sign = world.getSignAt(signLoc);
			if (sign == null) {
				continue;
			}
			List<String> lines = sign.getLines();
			if (lines.get(0).equalsIgnoreCase("[lift down]")) {
				return signLoc;
			}
			if (lines.get(0).equalsIgnoreCase("[lift up]")) {
				return signLoc;
			}
		}
		return null;
	}

	private boolean safeLoc(MC_Location loc) {
		return Pl3xLibs.getWorld(loc.dimension).getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).isSolid();
	}
}
