package net.pl3x.pl3xlift.runnables;

import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class TeleportPlayer extends Pl3xRunnable {
	private MC_Player player;
	private MC_Location location;

	public TeleportPlayer(MC_Player player, MC_Location location) {
		this.player = player;
		this.location = location;
	}

	@Override
	public void run() {
		if (player != null && location != null) {
			player.teleport(location);
		}
		player = null;
		location = null;
		this.cancel();
	}
}
